/**
 * 用户相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');


/**
 * Promise封装wx.checkSession
 */
function checkSession() {
  return new Promise(function(resolve, reject) {
    wx.checkSession({
      success: function() {
        resolve(true);
      },
      fail: function() {
        reject(false);
      }
    })
  });
}

/**
 * Promise封装wx.login
 */
function login() {
  return new Promise(function(resolve, reject) {
    wx.login({
      success: function(res) {
        if (res.code) {
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function(err) {
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
function loginByWeixin(userInfo) {
  console.log(userInfo)
  // 登录
  wx.login({
    success: res => {
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      console.log(res.code)
      wx.request({
        url: api.AuthLoginByWeixin,  // 后端路径
        data:{"scene":"mnp","code":res.code,"client":1},  // code
        header:{"content-type":"application/json","token":wx.getStorageSync("token")},
        method:"POST",
        success:function(res){
          console.log('登录信息')
          console.log(res)
          // 小程序端存储token
          wx.setStorageSync("token",res.data.data.token);
          // 获取用户信息保存
          wx.request({
            url: api.userInfoUrl,  // 后端路径
            data:{"userId":res.data.data.id},
            header:{"content-type":"application/json","token":wx.getStorageSync("token")},
            success:function(res2){
              console.log("个人信息");
              console.log(res2);
              wx.setStorageSync("userInfo",res2.data.data);
              resolve(res);
            }
          })

        }
      })
    }
  })


}

/**
 * 判断用户是否登录
 */
function checkLogin() {
  return new Promise(function(resolve, reject) {
    if (wx.getStorageSync('userInfo') && wx.getStorageSync('token')) {
      checkSession().then(() => {
        resolve(true);
      }).catch(() => {
        reject(false);
      });
    } else {
      reject(false);
    }
  });
}

/**
 * 加载情侣信息
 */

 function getShiYuSan(){
  
  return new Promise(function(resolve, reject) {
    let cid = 0;
    let userInfo = wx.getStorageSync('userInfo');
    console.log(userInfo)
    if(userInfo){
      cid= userInfo.id;
    }
    util.request(api.ShiYuSan,{id:cid}).then(function (res) {
      //判断当前用户是否具有空间发布
      if (userInfo.authDeploy == 1){
        wx.setStorageSync("cxauth",true);
      }
      // if (userInfo.id == res.data.shiliId){
      //   wx.setStorageSync("cxauth",true);
      // }
      // if (userInfo.id == res.data.sanliId){
      //   wx.setStorageSync("cxauth",true);
      // }
      // 获取数据成功
      if (res.code == 200) {
      var start_date = new Date();
      var end_date = new Date(res.data.shiyusan.replace(/-/g, "/"));
      var days = start_date.getTime() - end_date.getTime();
      var day = parseInt(days / (1000 * 60 * 60 * 24));
      res.data["sanYushiDay"] = day;
      wx.setStorageSync("sanYushi",res.data);
      resolve(true);
      } else {
        //获取数据失败，一般token失效
        wx.removeStorageSync('token');
        wx.removeStorageSync('userInfo');
        wx.removeStorageSync('sanYushi');
        wx.removeStorageSync('cxauth');
        reject(false);
      }
    });
  });
  
}

module.exports = {
  loginByWeixin,
  checkLogin,
  getShiYuSan
};