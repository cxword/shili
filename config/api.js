// 以下是业务服务器API地址
// 本机开发时使用
var WxApiRoot = 'http://localhost:8300/';
 
module.exports = {

  ImagUrl: WxApiRoot + 'api/uploads/', //图片识别地址

  // IndexUrl: WxApiRoot + 'index/list', //首页数据接口
  
  // UserIndex: WxApiRoot + 'user/index', //个人页面用户相关信息
  // Fabu: WxApiRoot + 'index/fabu', //发布
  // albumList: WxApiRoot + 'index/albumList', //相册列表
  // albumFabu: WxApiRoot + 'index/albumFabu', //相册发布
  // albumPhoto: WxApiRoot + 'index/albumPhoto', //图片列表
  // albumPhotoFabu: WxApiRoot + 'index/albumPhotoFabu', //图片列表
  

  AuthLoginByWeixin: WxApiRoot +  'api/login/check', //微信登录
  userInfoUrl: WxApiRoot + 'api/user/info', //获取个人信息
  userEditUrl: WxApiRoot + 'api/user/edit', //修改个人信息
  ShiYuSan: WxApiRoot + 'api/Shiliyusanli/detail', //十里与三里数据
  SayListUrl: WxApiRoot + 'api/say/list', //说说列表
  SayLabelUrl: WxApiRoot + 'api/label/list', //说说标签
  SayDianzanUrl: WxApiRoot + 'api/say/dianzan', //说说点赞
  SayLabelAddUrl: WxApiRoot + 'api/label/add', //新增说说标签
  uploadImageUrl: WxApiRoot + 'api/upload/image', //上传图片
  SayAddUrl: WxApiRoot + 'api/say/add', //发布说说

  albumList: WxApiRoot + 'api/albums/cateList', //相册列表
  albumAdd: WxApiRoot + 'api/albums/cateAdd', //相册新增
  albumDel: WxApiRoot + 'api/albums/cateDel', //相册删除
  albumPhoto: WxApiRoot + 'api/albums/albumList', //图片列表
  albumPhotoFabu: WxApiRoot + 'api/albums/albumAdd', //图片新增
  albumPhotoDel: WxApiRoot + 'api/albums/albumDel', //图片删除
  
};