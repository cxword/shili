var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp()

Page({
  data: {
    ImagUrl: api.ImagUrl,
    loadMoreStatus:"more",
    id: null,
    pageNo: 1,
    pageSize: 18,
    picList: [],
    imgList: [],
    userInfo: [],
    cxauth:false,
    toAlbumSend: true,
    toDeleAlbumSend: false,
    showLoading: false,
    checkboxChange: []
  },

  onLoad: function (options) {
    this.setData({
      toAlbumSend: true,
      toDeleAlbumSend: false,
    });
    if (options.id) {
      //获取用户的登录信息
    let cxauth = wx.getStorageSync("cxauth");
    let userInfo = wx.getStorageSync('userInfo');
      if (cxauth) {
        this.setData({
          userInfo: userInfo,
          cxauth: cxauth
        });
      } 
      this.setData({ id: parseInt(options.id)});
      this.getIssue();
    } else {
      wx.showToast({  title: '输入错误...即将跳转',  icon: 'loading', duration: 500});
      wx.navigateTo({ url: "/pages/index/index" });
    } 
  },
    /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    let pageNo = that.data.pageNo + 1
    that.setData({
      'pageNo': pageNo,
      loadMoreStatus: "more"
    });
    this.getIssue();
  },
  
  // 获取照片列表
  getIssue: function () {
    let that = this;
    var picList = that.data.picList;
    var imgList = that.data.imgList;
    util.request(api.albumPhoto, {
      cid: that.data.id,
      pageNo: that.data.pageNo,
      pageSize: that.data.pageSize
    }).then(function (res) {
      if (res.code === 200) {
        for(let i=0; i<res.data.lists.length; i++){
          imgList.push(that.data.ImagUrl+res.data.lists[i].uri);
          picList.push(res.data.lists[i]);
        }
        that.setData({
          picList: picList,
          imgList: imgList,
          count: res.data.count,
          loadMoreStatus: "dixian"
        }); 
      } else{
        that.setData({
          loadMoreStatus: "noeo"
        });
      }
    });
  },

  //图片预览
  imgYu: function(e){
    var index = e.currentTarget.dataset.index;
    var imgList = this.data.imgList;
    wx.previewImage({
      current: imgList[index], 
      urls: imgList 
    })
  },

  //新增照片按钮
  toCreateAlbumPhoto:function(){
    let that = this;
      wx.navigateTo({
        url: "/pages/album/photofabu?id="+that.data.id
      });
  },

  //删除照片按钮
  toDeleAlbum:function(){
    this.setData({
      toAlbumSend: false,
      toDeleAlbumSend: true,
    });
  },

  //删除照片 - 提交
  toDeleAlbumSend:function(){
    let that = this;
    console.log(that.data.checkedList)
    if(that.data.checkedList.length>0){
      util.modal('提示','是否删除选择中的照片？','确认','取消').then(res=>{
         //执行删除
         util.request(api.albumPhotoDel, {
          ids: that.data.checkedList,
          cid: 1
         }, 'POST').then(function(res) {
          if (res.code == 200) {
            wx.showToast({
              title: "删除成功！",
              icon:"none"
            });
            that.setData({
              'pageNo': 1,
              loadMoreStatus: "more"
            });
            that.onLoad();
    
          } else {
            wx.showToast({
              title: "删除失败！",
              icon:"none"
            })
          }
         })
      })
    } else {
      wx.showToast({
        title: "你还未选择照片哦！",
        icon:"none"
      })
    }
  },
  // 选择相册赋值
  checkboxChange(e){
    this.setData({
      checkedList: e.detail.value
    });
  }


 })