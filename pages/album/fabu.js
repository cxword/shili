var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp()

Page({
  data: {
  img_url: '',
  content:'',
  hasPicture: false,
  picUrls: '',
  hideAdd: 0,
  imgHide:1,
  cxauth: false
  },
  onLoad: function (options) {
    let cxauth = wx.getStorageSync("cxauth");
      if (cxauth) {
        this.setData({
          cxauth: cxauth
        });
      }
  },

  input:function(e){this.setData({content:e.detail.value})},
  chooseimage:function(){
  var that = this;
  // 图片选择
  wx.chooseImage({
    count: 1, // 默认9 
    sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有 
    sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有 
    success: function (res) {
      if (res.tempFilePaths.length>0){
        that.setData({hideAdd:1,img_url: res.tempFilePaths[0]})
      }
    }
  }) 
  },
  //发布按钮事件
  send:function(){
    var that = this;
    // wx.showLoading({ title: '上传中'})
    if(that.data.img_url != ''){
      that.img_upload();
    } else {
      console.log('木有上传封面');
      that.setData({
        hasPicture: true,
        picUrls: "image/20230203/4c66cded-0a7b-4d87-b68b-e8e3d4bc9cc6.png"
      })
      that.albumAdds();
    }
  },
  //图片上传
  img_upload: function () {
  let that = this;
  //由于图片只能一张一张地上传，所以用循环
   wx.uploadFile({
   url: api.uploadImageUrl,
   filePath: that.data.img_url,
   name: 'file',
   success: function (res) {
    var _res = JSON.parse(res.data);
    if (_res.code == 200){
      that.setData({
        hasPicture: true,
        picUrls: _res.data.url
      })
      that.albumAdds();
    } else {
      console.log('图片上传失败了');
    }
  }
  })
  },
  albumAdds: function () {
    let that = this;
    let id = wx.getStorageSync('userInfo').id;
    console.log("打印新建反馈id")
      console.log(id)

    util.request(api.albumAdd, {
     uid: id,
     name: that.data.content,
     image: that.data.picUrls,
     imgHide:that.data.imgHide,
     pid: 0,
     type: 10
    }, 'POST').then(function(res) {
      console.log("打印新建反馈")
      console.log(res)
      if (res.code == 200) {
        wx.showModal({
          title: '提交成功',
          showCancel: true,
          })
        wx.navigateBack();
      } else {
        wx.showModal({
          title: '提交失败',
          showCancel: false,
          })
      }
    });
   },
   radioChange: function (e) {
    //是否其他人可见
    this.setData({
      imgHide: e.detail.value
    })
  }
 })