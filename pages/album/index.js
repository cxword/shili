var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp()

Page({
  data: {
    ImagUrl: api.ImagUrl,
    loadMoreStatus: "loading",
    picList: [],
    cxauth: false,
    userInfo: [],
    showLoading:true,
    toAlbumSend: true,
    toDeleAlbumSend: false,
    radioChange:''
  },

  onLoad: function (options) {
    this.setData({
      toAlbumSend: true,
      toDeleAlbumSend: false,
    });
  },
   /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getIssue();
  },

  onShow: function () {
    //获取用户的登录信息
    let cxauth = wx.getStorageSync("cxauth");
    let userInfo = wx.getStorageSync('userInfo');
    if (cxauth) {
      this.setData({
        userInfo: userInfo,
        cxauth: cxauth
      });
    } 
    this.getIssue();
  },

  //相册分类查询
  getIssue: function () {
    let that = this;
    let userInfo= wx.getStorageSync('userInfo');
    let authDeploy = -1;
    if(userInfo.authDeploy != null){
      authDeploy= userInfo.authDeploy
    }
    util.request(api.albumList,{
      authDeploy: authDeploy
     }).then(function (res) {
      if (res.code == 200) {
        that.setData({
          picList: res.data,
          loadMoreStatus: "dixian",
        });
      }
    });
  },

  //新增相册按钮
  toCreateAlbum:function(){
      wx.navigateTo({
        url: "/pages/album/fabu"
      });
  },
  //删除相册按钮
  toDeleAlbum:function(){
    this.setData({
      toAlbumSend: false,
      toDeleAlbumSend: true,
    });
  },
  //删除相册  - 提交
  toDeleAlbumSend:function(){
    let that = this;
    console.log(that.data.radioChange)
    if(that.data.radioChange.length>0){
      util.modal('提示','删除相册会删除相册所有照片哦！','确认','取消').then(res=>{
         //执行删除
         util.request(api.albumDel, {
          cid: that.data.radioChange
         }, 'POST').then(function(res) {
          console.log("执行删除")
          console.log(res)
          if (res.code == 314) {
            wx.showToast({
              title: "内有照片不能删除哦！",
              icon:"none"
            })
          }else if (res.code == 200) {
            wx.showToast({
              title: "删除成功！",
              icon:"none"
            })
            that.onLoad();
          } else {
            wx.showToast({
              title: "删除失败！",
              icon:"none"
            })
          }
         })
      })
    } else {
      wx.showToast({
        title: "你还未选择相册哦！",
        icon:"none"
      })
    }
  },
  
  toDetail:function(e){
    //进入动态详情页
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `/pages/album/albumimg?id=${id}`,
    })
  },
  // 选择相册赋值
  radioChange(e){
    this.setData({
      radioChange: e.detail.value
    });
  }
 })