var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp()

Page({
  data: {
  content:'',
  hasPicture: false,
  imageArray: [],
  imageArray2: [],
  tagList:[],
  imagesOne:""
  },
  onLoad: function (options) {
    let cxauth = wx.getStorageSync("cxauth");
      if (cxauth) {
        this.setData({
          cxauth: cxauth
        });
      }
    // 默认读取标签
    this.syaLabel();
  },
  // 默认读取标签
  syaLabel:function(){
    let that = this
    util.request(api.SayLabelUrl).then(function(res) {
      that.setData({
          tagList: res.data.lists
        })
      })
  },
  // 点击标签
  handleTag(e) {
    let index = e.currentTarget.dataset.index
    let tagList = this.data.tagList
    tagList[index].check = tagList[index].check ? false : true
    this.setData({
      tagList: tagList
    })
  },
  // 填报动态信息
  input:function(e){
    this.setData({
      content:e.detail.value
    })
  },

  // 图片选择
  chooseimage:function(){
    var that = this;
    wx.chooseImage({
      count: 9, // 默认9 
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有 
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有 
        success: function (res) {
          if (res.tempFilePaths.length>0){
            //图如果满了9张，不显示加图
            if (res.tempFilePaths.length == 9){
                that.setData({hideAdd:1})
            }
            //把每次选择的图push进数组
            let img_url = [];
            for (let i = 0; i < res.tempFilePaths.length; i++) {
              img_url.push(res.tempFilePaths[i]);
            }
            that.setData({imageArray: img_url})
          }else{
            return wx.showToast({
              title: '图片未选择，请重新选择！',
              icon:"none"
            })
          }
        }
    }) 
  },

  //发布按钮事件
  send:function(){
  var that = this;
  let imgArray = that.data.imageArray;
  
  // 验收动态是否填写
  if(that.data.content===''){
    return wx.showToast({
      title: '请输入动态信息！',
      icon:"none"
    })
  }
  console.log(imgArray)
  // 验证图片是否选择
  if (imgArray.length>0){
    //上传图片
  //由于图片只能一张一张地上传，所以用循环
  for (let i = 0; i < imgArray.length; i++) {
    wx.uploadFile({
      filePath: imgArray[i],
      name: 'file',
      url: api.uploadImageUrl,
      success: function(res) {
        var _res = JSON.parse(res.data);
        console.log(_res);
        if (_res.code == 200){
          that.data.imagesOne = _res.data.url;
          that.data.imageArray2.push(_res.data.url)
          // that.data.imageArray2.push(_res.data.path)
          if(i === imgArray.length-1){
            //图片上传完后进行文本添加
            that.submitD();
          }
        } 
      }
    });
  }
  } else {
    that.submitD();
  }
  
  },
  submitD: async function () {
    var that = this;
    var imgArray = that.data.imageArray2;
    //判断内容
    if(!wx.getStorageSync('userInfo')){
      return wx.showToast({
        title: '登录出现了错误，请尝试重新登录！',
        icon:"none"
      })
    }
  
    // 标签
    var dynamicTag = null;
    that.data.tagList.forEach(item => {
      if (item.check) {
        dynamicTag=JSON.stringify(item.name);
      }
    })
    //循环上传完 发布说说
    util.request(api.SayAddUrl, {
      content: that.data.content,
      image: imgArray[0],
      images: JSON.stringify(imgArray),
      sysLabel: dynamicTag,
      userId: wx.getStorageSync('userInfo').id,
      author: wx.getStorageSync('userInfo').nickname
      },  "POST")
      .then(function(res) {
        console.log(res);
        if (res.code == 200){
          that.timer = setTimeout(() => {
              wx.navigateBack({
                delta: 1
              })
          }, 1000)
        } 
        console.log(res);
      });
  },

  

 })