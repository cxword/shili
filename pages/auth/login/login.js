var api = require('../../../config/api.js');
var util = require('../../../utils/util.js');
var user = require('../../../utils/user.js');


var app = getApp();
Page({
  onLoad: function(options) {
    // 页面初始化 options为页面跳转所带来的参数
    // 页面渲染完成

  },
  onReady: function() {

  },
  onShow: function() {
    // 页面显示
  },
  onHide: function() {
    // 页面隐藏

  },
  onUnload: function() {
    // 页面关闭

  },
  wxLogin: function(e) {
    if (e.detail.userInfo == undefined) {
      app.globalData.hasLogin = false;
      util.showErrorToast('微信登录失败');
      return;
    }
    // wx.removeStorageSync('token');
    // wx.removeStorageSync('userInfo');

    // 登录
  wx.login({
    success: res => {
      // 发送 res.code 到后台换取 openId, sessionKey, unionId
      console.log(res.code)
      wx.request({
        url: api.AuthLoginByWeixin,  // 后端路径
        data:{"scene":"mnp","code":res.code,"client":1},  // code
        header:{"content-type":"application/json","token":wx.getStorageSync("token")},
        method:"POST",
        success:function(res){
          console.log('登录信息')
          console.log(res)
          // 小程序端存储token
          wx.setStorageSync("token",res.data.data.token);
          // 获取用户信息保存
          wx.request({
            url: api.userInfoUrl,  // 后端路径
            data:{"userId":res.data.data.id},
            header:{"content-type":"application/json","token":wx.getStorageSync("token")},
            success:function(res2){
              console.log("个人信息");
              console.log(res2);
              wx.setStorageSync("userInfo",res2.data.data);
              // 切换到登录页面
            wx.navigateTo({
              url: '/pages/index/index'
            });
            }
          })
          // 获取三里十里全局信息
          

        }
      })
    }
  })

    // user.checkLogin().catch(() => {
    //   user.loginByWeixin(e.detail.userInfo).then(res => {
    //     app.globalData.hasLogin = true;
    //     wx.navigateBack({
    //       delta: 1
    //     })
    //   }).catch((err) => {
    //     app.globalData.hasLogin = false;
    //     util.showErrorToast('微信登录失败');
    //   });

    // });
  },
  accountLogin: function() {
    wx.navigateTo({
      url: "/pages/auth/accountLogin/accountLogin"
    });
  }
})