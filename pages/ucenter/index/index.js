var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');
var user = require('../../../utils/user.js');
var app = getApp();

Page({
  data: {
    ImagUrl: api.ImagUrl,
    userInfo: [],
    hasLogin: false,
    showLoading: true,
    newlable: true,
    style: 2,
    btns2: [{
      name: '资料设置',
      fun: 'toSetting',
      icon: '/static/images/icon/common/edit.png'
    },{
      name: '清除缓存',
      fun: 'clearStorage',
      icon: '/static/images/icon/common/del.png'
    },{
      name: '退出应用',
      fun: 'exitSys',
      icon: '/static/images/icon/common/exit.png'
    },{
      name: '婚礼邀请函',
      fun: 'wedding',
      icon: '/static/images/icon/common/riji.png'
    },]
  },

  onLoad: function() {

    // this.getIssue();
  },
  onReady: function() {
 
  },

  onShow: function() {
    this.getIssue();
  },
  // 读取个人中心数据
   getIssue: function () {
    let that = this;
    let btn = that.data.btns2;

    let userInfo = wx.getStorageSync('userInfo'); 
    that.setData({
        userInfo: userInfo,
        showLoading: false
      });
    if(!userInfo){
      // 个人中心进行一次更新
      util.request(api.userInfoUrl, {
        "userId":userInfo.id
      }).then(function (res) {
        res.data["avatar2"] = that.data.ImagUrl+res.data["avatar"];
        wx.setStorageSync("userInfo",res.data.data);
        that.setData({
          userInfo: res.data,
          showLoading: false
        });
      })
    } else {
      that.setData({
        userInfo: userInfo,
        showLoading: false
      });
    }
    // 加载菜单
    // 防止重复，添加判断
    if (that.data.newlable) {
      if (userInfo.authDeploy == 1) {
        btn.push({
          name: '标签管理',
          fun: 'toAnniversary',
          icon: '/static/images/icon/common/riji.png'
        });
      }
      that.setData({
        btns2: btn,
        showLoading: false,
        newlable: false
      })
    }
   },
   /**
   * 按钮统一点击事件
   * @param {*} event 
   */
  tapButton(event){
    let index = event.currentTarget.dataset.index;
    let item = {};
    let that = this;
    item = that.data.btns2[index];
    if (item.fun) {
      that[item.fun]();
    }
  },

  /**
   * 跳转到用户设置页面
   */
  toSetting: function () {
    // 判断用户是否登录
    let that = this;
    let userinfo = that.data.userInfo;
    if(userinfo){
      wx.navigateTo({
        url: '/pages/ucenter/editProfile/editProfile'
      })
    } else {
      wx.navigateTo({
        url: '/pages/ucenter/welcome/welcome'
      })
    }
  },

  /**
   * 跳转到婚礼邀请函
   */
  wedding: function () {
    wx.navigateTo({
      url: '/pages/wedding/wedding'
    })
  },
  /**
   * 跳转到标签管理
   */
  toAnniversary: function () {
    wx.navigateTo({
      url: '/pages/ucenter/label/label'
    })
  },
  /**
   * 清除小程序缓存
   */
  clearStorage: function () {
    console.log("清除小程序缓存");
    wx.showModal({
      title: '操作确认',
      content: '确定要清除所有缓存吗',
      success: function(res) {
      if (res.confirm) {
        wx.clearStorage({
          success: (res) => {
            util.modal('缓存已清理','为了保证数据正常，请退出应用后重启小程序');
          },
        })
        wx.navigateTo({
          url: "/pages/ucenter/index/index"
        });
      } else if (res.cancel) {
      console.log('用户点击取消')
      }
      }
      })
    // util.modal('操作确认','确定要清除所有缓存吗','清除缓存','取消').then(res=>{
    //   wx.clearStorage({
    //     success: (res) => {
    //       util.modal('缓存已清理','为了保证数据正常，请退出应用后重启小程序');
    //     },
    //   })
    // })
  },
   /**
   * 退出小程序
   */
  exitSys: function () {
    wx.showModal({
      title: '操作确认',
      content: '确定要退出小程序吗',
      success: function(res) {
      if (res.confirm) {
        wx.exitMiniProgram();
      } else if (res.cancel) {
      console.log('用户点击取消')
      }
      }
      })
    // modal('操作确认','确定要退出小程序吗','退出','取消').then(res=>{
    //   wx.exitMiniProgram();
    // })
  },
  goLogin: function () {
    let userinfo = this.data.userInfo;
    if(userinfo){
      console.log("已登录")
    } else {
      wx.navigateTo({
        url: '/pages/ucenter/welcome/welcome'
      })
    }
    
  },
  // bindPhoneNumber: function(e) {
  //   if (e.detail.errMsg !== "getPhoneNumber:ok") {
  //     // 拒绝授权
  //     return;
  //   }
  //   if (!this.data.hasLogin) {
  //     wx.showToast({
  //       title: '绑定失败：请先登录',
  //       icon: 'none',
  //       duration: 2000
  //     });
  //     return;
  //   }

  //   util.request(api.AuthBindPhone, {
  //     iv: e.detail.iv,
  //     encryptedData: e.detail.encryptedData
  //   }, 'POST').then(function(res) {
  //     if (res.errno === 0) {
  //       wx.showToast({
  //         title: '绑定手机号码成功',
  //         icon: 'success',
  //         duration: 2000
  //       });
  //     }
  //   });
  // },
  // goAfterSale: function() {
  //   wx.showToast({
  //     title: '目前不支持',
  //     icon: 'none',
  //     duration: 2000
  //   });
  // },
 
  exitLogin: function() {
    wx.showModal({
      title: '',
      confirmColor: '#b4282d',
      content: '退出登录？',
      success: function(res) {
        if (!res.confirm) {
          return;
        }
        util.request(api.AuthLogout, {}, 'POST');
        app.globalData.hasLogin = false;
        wx.removeStorageSync('token');
        wx.removeStorageSync('userInfo');
        wx.removeStorageSync('cxauth');
        wx.reLaunch({
          url: '/pages/index/index'
        });
      }
    })

  }
})