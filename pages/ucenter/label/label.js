var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

var app = getApp()

Page({
  data: {
    content:'',
    tagList:[]
  },
  onLoad: function (options) {
    // 默认读取标签
    this.syaLabel();
  },
  // 默认读取标签
  syaLabel:function(){
    let that = this
    util.request(api.SayLabelUrl).then(function(res) {
      that.setData({
        tagList: res.data.lists
      })
    })
  },

  // 填报标签信息
  input:function(e){
    this.setData({
      content:e.detail.value
    })
  },

  //发布按钮事件
  send:function(){
    var that = this;
    // 验收动态是否填写
    if(that.data.content===''){
      return wx.showToast({
        title: '请输入填写标签信息！',
        icon:"none"
      })
    }

    //新增标签
    util.request(api.SayLabelAddUrl, {
      name: that.data.content,
      sort:50,
      isShow:1
      },  "POST")
      .then(function(res) {
        if (res.code == 200){
          wx.showToast({
            title: "添加成功！",
            icon:"none"
          })
          this.setData({
            content:''
          })
          that.syaLabel();
        } else {
          wx.showToast({
            title: "添加失败...",
            icon:"none"
          })
        }
      });
  },

  

 })