var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ImagUrl: api.ImagUrl,
    userInfo: [],
    hasUserInfo: false,
    token:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let userInfo = wx.getStorageSync('userInfo');
    if (userInfo) {
      wx.navigateTo({
        url: '/pages/ucenter/index/index',
      })
      return
    }
  },

  // 点击登录
  userLogin: function() {
    let that = this;
    that.setData({
      hasUserInfo: true
    })
    // 登录
    wx.login({
      success: res => {
        console.log(res)
        // 发送 res.code 到后台换取 openId, sessionKey, unionId  //wx3030d8569f003722
        wx.request({
          url: api.AuthLoginByWeixin,  // 后端路径
          data:{"scene":"mnp","code":res.code,"client":1},  // code
          header:{"content-type":"application/json"},
          method:"POST",
          success:function(res){
            // 小程序端存储token
            wx.setStorageSync("token",res.data.data.token);
            // 获取用户信息保存
            wx.request({
              url: api.userInfoUrl,  // 后端路径
              data:{"userId":res.data.data.id},
              header:{"content-type":"application/json","token":wx.getStorageSync("token")},
              success:function(res2){
                if(res2.data.code == 200){
                  res2.data.data["avatar2"] = that.data.ImagUrl+res2.data.data["avatar"];
                  if(res2.data.data["authDeploy"] == 1){
                    wx.setStorageSync("cxauth",true);
                  }
                  wx.setStorageSync("userInfo",res2.data.data);
                  wx.navigateBack();
                } else {
                  wx.showToast({
                    title: '登录失败',
                    icon: 'error',
                    duration: 2000
                  });
                }
                
              }
            })
          }
        })
      }
    })
    console.log("登录")
    console.log(wx.getStorageSync("userInfo"))
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  
  // getUserProfile: async function (e) {
  //   //获取用户的信息
  //    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
  //    wx.getUserProfile({
  //     lang: 'zh_CN',
  //     desc: '获取您的昵称、头像及性别', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
  //     success: (res) => {
  //         // this.setData({
  //         // userInfo: res.userInfo,
  //         // hasUserInfo: true
  //         // })
  //         console.log(res);
  //     }
  //     })

  // }
})
