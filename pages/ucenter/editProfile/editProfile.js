var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

const app = getApp();


Page({

  /**
   * 页面的初始数据
   */
  data: {
    ImagUrl: api.ImagUrl,
    img_url: '',
    hideAdd: 0,
    userInfo: [],
    avatars: '',
    sexArray: ['未知','男', '女']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    let userInfo = wx.getStorageSync('userInfo');
    if (!userInfo) {
      wx.navigateTo({
        url: '/pages/welcome/welcome',
      })
      return
    }
    that.setData({
      userInfo: userInfo
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  // 选择图片
  uploadHeadPortrait:function(){
    var that = this;
    // 图片选择
    wx.chooseImage({
      count: 1, // 默认9 
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有 
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有 
      success: function (res) {
        if (res.tempFilePaths.length>0){
          that.setData({hideAdd:1,img_url: res.tempFilePaths[0],[`userInfo.avatar`]:res.tempFilePaths[0]})
        }
      }
    }) 
    },
  // 修改用户资料
  updateUser: async function (e) {
    var that = this;
    //判断是否修改图片
    if(that.data.hideAdd == 1){
    //先执行图片上传
    wx.uploadFile({
      url: api.uploadImageUrl,
      filePath: that.data.img_url,
      name: 'file',
      success: function (res) {
        var _res = JSON.parse(res.data);
        if (_res.code == 200){
          that.setData({
            avatars: _res.data.url
          })
          that.tijiao();
        }
      }
    })
    } else {
      that.tijiao();
    }
  },
  tijiao: function () {
    var that = this;
    var sex = 0;
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    //执行修改个人信息
    if (that.data.userInfo.nickname == null || that.data.userInfo.nickname == '') {
      wx.showModal({
        title: '请输入昵称',
        showCancel: false,
      })
      return;
    }
    if (!myreg.test(that.data.userInfo.mobile)) {
      wx.showModal({
        title: '联系方式错误',
        showCancel: false,
      })
      return;
    }

    switch(that.data.userInfo.sex){
      case "男":
        that.setData({
          [`userInfo.sex`]:1
        })
        sex = 1;
        break;
      case "女":
        that.setData({
          [`userInfo.sex`]:2
        })
        sex = 2;
        break;
      default:
        that.setData({
          [`userInfo.sex`]:0
        })
        sex = 0;
    }
    util.request(api.userEditUrl, {
      userId: that.data.userInfo.id,
      avatar: that.data.avatars,
      nickname: that.data.userInfo.nickname,
      mobile: that.data.userInfo.mobile,
      sex: that.data.userInfo.sex,
      field: "nickname",
      value: that.data.userInfo.nickname
     }, 'POST').then(function(res) {
       if (res.code == 200) {
          // 更新个人信息 
          wx.request({
            url: api.userInfoUrl,  // 后端路径
            data:{"userId":that.data.userInfo.id},
            header:{"content-type":"application/json","token":wx.getStorageSync("token")},
            success:function(res2){
              res2.data.data["avatar2"] = that.data.ImagUrl+res2.data.data["avatar"];
              wx.setStorageSync("userInfo",res2.data.data);
            }
          })
          wx.showModal({
            title: '提交成功',
            showCancel: true,
          })
         wx.navigateBack();
       } else {
          wx.showModal({
            title: '提交失败',
            showCancel: false,
          })
       }
     });
  },
  userNameChange: function (e) {
    this.setData({
      [`userInfo.nickname`]: e.detail.value
    })
  },
  mobileChange: function (e) {
    //修改联系方式
    this.setData({
      [`userInfo.mobile`]: e.detail.value
    })
  },
  radioChange: function (e) {
    //性别修改
    this.setData({
      [`userInfo.sex`]: e.detail.value
    })
  }
})