var api = require('../../config/api.js');
var util = require('../../utils/util.js');
var user = require('../../utils/user.js');
var app = getApp()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    ImagUrl: api.ImagUrl,
    loadMoreStatus: "loading",
    xyxList: [],  //情侣数据
    dailyList: [], // 说说数据
    count: 0, // 说说总数
    pageNo: 1,
    pageSize: 20,
    userInfo: [],
    cxauth: false,
    // dianzanArray: ['/static/images/icon/zan_one.png','/static/images/icon/zan_two.png'],
    dianzan: '/static/images/icon/zan_one.png'
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    that.setData({
      'pageNo': 1,
      'dailyList':[],
      loadMoreStatus: "more"
    });
    this.getIssue();
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this;
    that.setData({
      'pageNo': 1,
      'dailyList':[],
      loadMoreStatus: "more"
    });
    this.getIssue();
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let that = this;
    let pageNo = that.data.pageNo + 1
    that.setData({
      'pageNo': pageNo,
      loadMoreStatus: "more"
    });
    this.getIssue();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },

  // 说说列表
  getIssue: function () {
    let that = this;
    // 加载情侣数据
    try {
      var sanYushi = wx.getStorageSync("sanYushi")
      var cxauth = wx.getStorageSync("cxauth");
      console.log(cxauth)
      if (!sanYushi) {
          user.getShiYuSan().then(res => {
            var sanYushi = wx.getStorageSync("sanYushi")
            that.setData({
              xyxList: sanYushi,
              cxauth: cxauth
            });
          })
      } else {
        //已有数据不重复读取
        that.setData({
          xyxList: sanYushi,
          cxauth: cxauth
        });
        console.log("已有数据不重复读取！");
      }
    } catch (e) {
      // 提示情侣数据错误
      console.log("情侣数据读取错误！");
    }

    // 加载说说列表数据

    var dailyList = that.data.dailyList;
    
    util.request(api.SayListUrl, {
      pageNo: that.data.pageNo,
      pageSize: that.data.pageSize
    }).then(function (res) {
      if (res.code === 200) {
        for(let i=0; i<res.data.lists.length; i++){
          let imgesss = [];
          if (res.data.lists[i].images !=null) {
            res.data.lists[i]["imagess"] = [];
            imgesss = JSON.parse(res.data.lists[i].images);
            res.data.lists[i]["imagess"]=imgesss;
          }
          dailyList.push(res.data.lists[i])
        }
        that.setData({
          dailyList: dailyList,
          count: res.data.count,
          loadMoreStatus: "dixian"
        }); 
      } else {
        that.setData({
          loadMoreStatus: "noeo"
        });
      }
    });
  },

  //预览图片
  previewImg:function(e){
    var that = this;
    var cximages = [];
    //预览图片
    let index = e.currentTarget.dataset.index
    let images = e.currentTarget.dataset.src
    // 循环给images+地址
    for(let i=0; i<images.length; i++){
      cximages.push(that.data.ImagUrl+images[i]);
    }
    wx.previewImage({
      current:cximages[index],
      urls: cximages,
    }) 
  },

  // 点击发布按钮
  fabu: function (event) {
    //获取用户的权限
    var cxauth = wx.getStorageSync("cxauth")
    if (app.globalData.hasLogin) {
      if(cxauth){
        wx.navigateTo({
          url: "/pages/fabu/fabu"
        });
      } else {
        wx.navigateTo({
          url: "/pages/auth/login/login"
        });
      };
    } else {
      console.log("不具备发布功能！");
    }
  },
  
  // 点击点赞
  dianzan: function (event) {
    let that = this;
    // 判断是否登录
    let userInfo = wx.getStorageSync('userInfo');
    var dailyList = that.data.dailyList;
    let sid = event.currentTarget.dataset.index;
    let inds = event.currentTarget.dataset.inds;
    //判断当前用户是否具有空间
    if(userInfo){
      //提交点赞数据
      util.request(api.SayDianzanUrl, {
      sid: sid,
      uid: userInfo.id
      },  "POST")
      .then(function(res) {
        if (res.code == 200){
          // 修改图标
          if(dailyList[inds].saylike == null){
            dailyList[inds].saylike = [];
          }
          dailyList[inds].saylike.push(userInfo.avatar);
          that.setData({
            dailyList: dailyList
          });
          wx.showToast({
            title: "谢谢你的点赞",
            icon:"none"
          })
        }else{
          wx.showToast({
            title: res.msg,
            icon:"none"
          })  
        }
      });
    } else {
      wx.showModal({
        title: '点赞需要登录',
        content: '是否跳转登录？',
        success: function(res) {
          if (res.confirm) {
            wx.navigateTo({
              url: '/pages/ucenter/welcome/welcome'
            });
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  }
})